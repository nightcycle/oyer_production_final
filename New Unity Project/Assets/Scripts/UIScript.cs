﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIScript : MonoBehaviour
{
	public GameObject timer;
	public GameObject YouWin;
	public GameObject restart;
	
	public float time_left = 120;
	public GameObject RV;
	public static bool game_result = false;
	public static bool you_win = true;
	// Start is called before the first frame update
	void Start()
	{
		
	}

	// Update is called once per frame
	void Update()
	{
		time_left = time_left - Time.deltaTime;
		timer.GetComponent<Text>().text = Mathf.Round(time_left).ToString();
		if (time_left < 0){
			time_left = 0;
			game_result = true;
		}
		if (game_result == true){
			restart.SetActive (true);
			if (you_win == true){
				YouWin.SetActive(true);
			}
		}
		if (Input.GetButtonDown("Submit")){
			restart.SetActive (false);
			YouWin.SetActive(false);
			game_result = false;
			you_win = true;
			Application.LoadLevel(Application.loadedLevel);
		}
	}
}
