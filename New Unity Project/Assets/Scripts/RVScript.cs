﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RVScript : MonoBehaviour
{
	Transform current_road_transform; //road tha t the RV is currently above
	Transform closest_intersection_transform; //closest intersection transform
	//float inter_dist; //the distance to the closest intersection
	public GameObject intersections; //grouping holding all the intersections
	//Transform camera; //camera
	//Transform ints_trans; //the transform of intersections
	Quaternion move_options = new Quaternion(0,0,0,0);//move_options
	
	//balancing variables
	//float intersect_detect_dist = 0.15f;//how close the RV has to be to an intersection for stuff to happen
	float outreach = 5f; //distance checked in each direction
	float RV_orig_speed = 0.0007f; //how much the RV should change each frame
	float speed; //speed adjusted for framerate
	
	void Start(){
		speed = RV_orig_speed; 
		//ints_trans = intersections.transform;
		//camera = transform.Find("Main Camera").transform;
	}
	
	void debug_point(Vector3 pos, Color col){
		Debug.DrawLine(new Vector3(pos.x, pos.y,-2), new Vector3(pos.x, pos.y,100), col);
	}

	Transform get_road_trans_below(Vector3 check_position){//gets any roads directly below the point
		RaycastHit hit;
		Vector3 foward_pos = new Vector3(check_position.x, check_position.y, 20f);
		Ray ray = new Ray(check_position, new Vector3(0,0,1));
		
		if (Physics.Raycast(ray, out hit, 100.0f) && hit.transform){

			debug_point(hit.transform.position, Color.white);
			return hit.transform;
		}
		return null;
	}
	
	bool is_road_below(Vector3 check_position){
		RaycastHit hit;
		Vector3 foward_pos = new Vector3(check_position.x, check_position.y, 20f);
		Ray ray = new Ray(check_position, new Vector3(0,0,1));
		
		if (Physics.Raycast(ray, out hit, 100.0f) && hit.transform){
			if (hit.transform.gameObject.layer == 9){
				return true;
			}
		}
		return false;
	}
	
	void set_current_road_transform(){//sets road currently under RV
		Vector3 current_position = new Vector3(transform.position.x, transform.position.y, -5);
		Transform road_trans = get_road_trans_below(current_position);
		if (road_trans){
			current_road_transform = road_trans;
		}
	}
	/*
	void set_closest_intersection_transform(){//goes through all the intersections and finds the nearest
		inter_dist = 1000000000;
		foreach (Transform intersection in ints_trans){
			float dist = get_v2_distance(v3_to_v2(intersection.position), v3_to_v2(transform.position));
			if (dist < inter_dist){
				closest_intersection_transform = intersection;
				//print("Closest Intersection" + intersection.position);
				inter_dist = dist;
			}
		}	
	}
	*/
	
	float get_v2_distance(Vector2 point1, Vector2 point2){//gets distance apart from two V2s
		return Mathf.Sqrt(Mathf.Pow(point1.x + point2.x,2) + Mathf.Pow(point1.y + point2.y, 2));
	}

	Vector2 v3_to_v2(Vector3 point){ //converts a V3 to a V2
		return new Vector2(point.x, point.y);
	}

	// Update is called once per frame
	void Update()
	{   
		/*
		set_current_road_transform();//determine what street you're on	
		set_closest_intersection_transform();//set the nearest intersection
		
		if (current_road_transform && inter_dist > intersect_detect_dist){//is the RV on straight road
			
			//set the axis the RV can move
			if (current_road_transform.eulerAngles.z == 90){
				move_options = new Quaternion(1,1, 0, 0);
			}else{
				move_options = new Quaternion(0,0, 1, 1);
			}
		}else if (inter_dist <= intersect_detect_dist){//what to do if you're at an intersection							
			
			//if you are on an intersection determine the directions you can drive/change current road	
			if (get_road_trans_below(closest_intersection_transform.position + new Vector3(outreach,0,0))){
				move_options.x = 1;
				print("alternate x route detected");
			}else if (get_road_trans_below(closest_intersection_transform.position + new Vector3(-outreach,0,0))){
				move_options.y = -1;
				print("alternate -x route detected");	
			}else if (get_road_trans_below(closest_intersection_transform.position + new Vector3(0,outreach,0))){
				move_options.z = 1;
				print("alternate y route detected");
			}else if (get_road_trans_below(closest_intersection_transform.position + new Vector3(0,-outreach,0))){
				move_options.w = -1;
				print("alternate -y route detected");	
			}								
		}

		//determine if there is any keyboard input that corresponds to the available directions
		float move_x = Input.GetAxis ("Horizontal"); //which way the player is inputting
		float move_y = Input.GetAxis ("Vertical");	//which way the player is inputting
		Vector2 throttle = new Vector2(0,0); //which way the RV is pushed
		
		//set throttle x
		print(move_x);
		if (move_x > 0 && move_options.y > 0){
			throttle.x = 1;
		}else if(move_x < 0 && move_options.x > 0){
			throttle.x = -1;	
		}

		//set throttle y
		if (move_y > 0 && move_options.z > 0){
			throttle.y = 1;
			throttle.x = 0; //because there are no diagonal roads
		}else if(move_y < 0 && move_options.w > 0){
			throttle.y = -1;	
			throttle.x = 0; //because there are no diagonal roads
		}
	
		//get new current road
		current_road_transform = get_road_trans_below(closest_intersection_transform.position + new Vector3(throttle.x*outreach,throttle.y*outreach,-1));
		debug_point(current_road_transform.position, Color.red);
		debug_point(closest_intersection_transform.position, Color.yellow);
		debug_point(closest_intersection_transform.position + new Vector3(throttle.x*outreach,throttle.y*outreach,-1), Color.cyan);
		if (current_road_transform){
			//center on current road + move
			print("Move ops: "+move_options);
			print("Throttle: "+throttle);
			print("Current road pos: "+current_road_transform.position);
			if (throttle.x != 0){
				transform.position = new Vector3(transform.position.x, current_road_transform.position.y, transform.position.z);	
				//transform.eulerAngles = new Vector3(0, 0, 90+throttle.x*90);
				//camera.eulerAngles = new Vector3(0, 0, -throttle.x*90);
			}else if(throttle.y != 0){
				transform.position = new Vector3(current_road_transform.position.x, transform.position.y, transform.position.z);	
				//transform.eulerAngles = new Vector3(0, 0, 180+(throttle.x*90));
				//camera.eulerAngles = new Vector3(0, 0, 90+(-throttle.x*90));
			}
			
			transform.position = transform.position + new Vector3(speed*throttle.x, speed*throttle.y, 0);
		}
		*/
		//determine if there is any keyboard input that corresponds to the available directions
		speed = RV_orig_speed/Time.deltaTime;
		float move_x = Input.GetAxis ("Horizontal"); //which way the player is inputting
		float move_y = Input.GetAxis ("Vertical");	//which way the player is inputting
		Vector2 throttle = new Vector2(0,0); //which way the RV is pushed
		
		//set throttle x
		//print(move_x);
		if (move_x > 0){
			throttle.x = 1;
		}else if(move_x < 0){
			throttle.x = -1;	
		}

		//set throttle y
		if (move_y > 0){
			throttle.y = 1;
			throttle.x = 0; //because there are no diagonal roads
		}else if(move_y < 0){
			throttle.y = -1;	
			throttle.x = 0; //because there are no diagonal roads
		}	
	
		//checks to see if there is more road in that direction
		//current_road_transform = null;
		debug_point(new Vector3(transform.position.x, transform.position.y, -1) + new Vector3(speed*throttle.x,speed*throttle.y,0), Color.yellow);
		//current_road_transform = get_road_trans_below(new Vector3(transform.position.x, transform.position.y, -1) + new Vector3(speed*throttle.x,speed*throttle.y,0));
		bool road_check = is_road_below(new Vector3(transform.position.x, transform.position.y, -1) + new Vector3(speed*throttle.x,speed*throttle.y,0));
		if (road_check == true){
			transform.position = transform.position + new Vector3(speed*throttle.x, speed*throttle.y, 0);
			if (throttle.y > 0){
				transform.eulerAngles = new Vector3(0,0,0);
				//camera.eulerAngles = new Vector3(0, 0, 90);
			}else if (throttle.y < 0){
				transform.eulerAngles = new Vector3(0, 0, 180);
				//camera.eulerAngles = new Vector3(0, 0, 270);
			}
			
			if (throttle.x > 0){
				transform.eulerAngles = new Vector3(0, 0, 270);
				//camera.eulerAngles = new Vector3(0, 0, 90);
			}else if (throttle.x < 0){
				transform.eulerAngles = new Vector3(0, 0, 90);
				//camera.eulerAngles = new Vector3(0, 0, 0);
			}
			//camera.eulerAngles = new Vector3(0, 0, 0);
		}
	}
	
}
