﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TornadoScript : MonoBehaviour {

	float direction_time = 0f;
	Vector3 direction = new Vector3(0f,0f,0f);
	float speed = 0f;
	Rigidbody rb;
	float correction_mass = 0.25f;
	
	public GameObject RV_obj;
	float RV_dist = 100000000000000000;
	public void set_new_heading(){
		direction = new Vector3(Random.Range(-10.0f, 10.0f), Random.Range(-10.0f, 10.0f), 0);
		direction_time = Random.Range(0.5f, 7.0f);
		speed = Random.Range(1.5f, 5.0f)/10f;
	}
	
	void Start(){
		set_new_heading();
		rb = GetComponent<Rigidbody>();
		rb.useGravity = false;
	}

	float get_v2_distance(Vector2 point1, Vector2 point2){//gets distance apart from two V2s
		return Mathf.Sqrt(Mathf.Pow(point1.x - point2.x,2) + Mathf.Pow(point1.y - point2.y, 2));
	}
	
	void Update(){
		direction_time -= Time.deltaTime;
		if (direction_time <= 0){set_new_heading();}
		rb.AddForce (direction * speed);
		RV_dist = get_v2_distance(RV_obj.transform.position, transform.position);
		print("Dist: " + RV_dist);
		rb.mass = 1;
		if (RV_dist < 0.7){
			UIScript.you_win = false;
			UIScript.game_result = true;
			RV_obj.SetActive(false);
		}
		if (RV_dist > 10){
			GetComponent<AudioSource>().volume = 0.1f;
		}else{
			GetComponent<AudioSource>().volume = 1.2f-(RV_dist/10);
		}
		
		if (transform.position.x < -24){
			//print("too far left");
			direction = new Vector3(Mathf.Abs(direction.x), direction.y, direction.z);
			rb.mass = correction_mass;
		}else if (transform.position.x > 24){
			//print("too far right");
			direction = new Vector3(-1*Mathf.Abs(direction.x), direction.y, direction.z);
			rb.mass = correction_mass;
		}
		if (transform.position.y > 13){
			//print("too far up");
			direction = new Vector3(direction.x, -1*Mathf.Abs(direction.y), direction.z);
			rb.mass = correction_mass;
		}else if (transform.position.y < -13){
			//print("too far down");
			direction = new Vector3(direction.x, Mathf.Abs(direction.y), direction.z);
			rb.mass = correction_mass;
		}
		foreach (Transform TSprite in transform){
			TSprite.Rotate(0,0,speed*5);
		}
	}
	/*
	void OnTriggerEnter(Collider other) {
		print("hit");
		if (other.gameObject.CompareTag ("RV"))
		{
			print("Is RV");
			other.gameObject.SetActive (false);
		}
	}
	*/
}
